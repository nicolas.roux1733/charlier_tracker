﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api.tools
{
    public class CharliePosition
    {
        public double x;
        public double y;

        public CharliePosition(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
