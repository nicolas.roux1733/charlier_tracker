﻿using System.Web.Http;
using Tobii.Interaction;
using Tobii.Interaction.Framework;
using System;
using NLog;

namespace api.Controller
{
    public class GameController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public status.LaunchRecordStatus GetLaunchRecord(int id, bool charlie)
        {
            if (TrackingData.host == null)
            {
                TrackingData.host = new Host();
            }

            var eyeTrackingStatus = TrackingData.host.States.GetEyeTrackingDeviceStatusAsync().Result;

            if (eyeTrackingStatus.IsValid)
            {
                switch (eyeTrackingStatus.Value)
                {
                    case EyeTrackingDeviceStatus.Tracking:
                        if (!TrackingData.InitTrackingData(id, charlie))
                        {
                            logger.Error("Level does not exist");
                            return new status.LaunchRecordStatus("LEVEL_" + id + "_DOES_NOT_EXIST");
                        };
                        return new status.LaunchRecordStatus("OK");
                    default:
                        logger.Error("Tracker error");
                        return new status.LaunchRecordStatus("TRACKER_ERROR");
                }
            }
            logger.Error("Unknow error");
            return new status.LaunchRecordStatus("ERROR");
        }
    }
}
