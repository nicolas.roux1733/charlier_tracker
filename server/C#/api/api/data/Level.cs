﻿using api.tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api.data
{
    public class Level
    {
        public int id;
        public CharliePosition charliePos;
        public CharliePosition feleciePos;
        public bool charlie;

        public Level(int id, CharliePosition charliePos, CharliePosition feleciePos)
        {
            this.id = id;
            this.charliePos = charliePos;
            this.feleciePos = feleciePos;
        }

        public CharliePosition getPosition()
        {
            return charlie ? charliePos : feleciePos;
        }
    }
}
