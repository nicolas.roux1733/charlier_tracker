﻿using System;
using Tobii.Interaction;
using RestSharp;
using api.data;
using System.Diagnostics;
using System.Collections.Generic;
using api.tools;
using NLog;

namespace api
{
    static public class TrackingData
    {
        // en pixel
        private const int PRECISION = 75;

        // en seconde
        private const int TIME_TO_FIND = 2;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private static Stopwatch stopwatch = new Stopwatch();

        private static Stopwatch duringTime = new Stopwatch();

        public static Dictionary<int, Level> levels = new Dictionary<int, Level>
            {
                { 1, new Level(1, new CharliePosition(1670d, 700d), new CharliePosition(1430d, 690d)) },
                { 2, new Level(2, new CharliePosition( 1380d, 350d ), new CharliePosition(600d, 680d)) },
                { 3, new Level(3, new CharliePosition( 530d, 760d ), new CharliePosition(1690d, 500d)) },
                { 4, new Level(4, new CharliePosition( 810d, 210d ), new CharliePosition(600d, 730d)) },
                { 5, new Level(5, new CharliePosition( 1530d, 150d ), new CharliePosition(1320d, 900d)) },
                { 6, new Level(6, new CharliePosition( 1450d, 330d ), new CharliePosition(725d, 915d)) },
                { 7, new Level(7, new CharliePosition( 270d, 760d ), new CharliePosition(1750d, 350d)) },
                { 8, new Level(8, new CharliePosition( 1750d, 600d ), new CharliePosition(413d, 739d)) },
                { 9, new Level(9, new CharliePosition( 1500d, 240d ), new CharliePosition(500d, 620d)) },
                { 10, new Level(10, new CharliePosition( 1800d, 450d ), new CharliePosition(80d, 800d)) },
                { 11, new Level(11, new CharliePosition( 1090d, 360d ), new CharliePosition(751d, 347d)) },
                { 12, new Level(12, new CharliePosition( 1240d, 200d ), new CharliePosition(994d, 270d)) },
                { 13, new Level(13, new CharliePosition( 160d, 740d ), new CharliePosition(541d, 604d)) },
                { 14, new Level(14, new CharliePosition( 1840d, 810d ), new CharliePosition(1834d, 424d)) },
                { 15, new Level(15, new CharliePosition( 1340d, 440d ), new CharliePosition(1146d, 716d)) },
                { 16, new Level(16, new CharliePosition( 1690d, 670d ), new CharliePosition(269d, 881d)) },
                { 17, new Level(17, new CharliePosition( 360d, 770d ), new CharliePosition(638d, 890d)) },
                { 18, new Level(18, new CharliePosition( 650d, 680d ), new CharliePosition(1075d, 813d)) },
                { 19, new Level(19, new CharliePosition( 180d, 100d ), new CharliePosition(1002d, 8624d)) },
                { 20, new Level(20, new CharliePosition( 650d, 180d ), new CharliePosition(586d, 520d)) }
            };

        public static Level currentLevel;

        public static int distanceInPixel = 0;

        public static GazePointDataStream stream;

        public static CharliePosition previousPos = null;

        public static Host host = null;

        private static bool IsInRangeX(double value)
        {
            return value >= currentLevel.getPosition().x - PRECISION && value <= currentLevel.getPosition().x + PRECISION;
        }

        private static bool IsInRangeY(double value)
        {
            return value >= currentLevel.getPosition().y - PRECISION && value <= currentLevel.getPosition().y + PRECISION;
        }

        private static void Initialize()
        {
            currentLevel = null;
            distanceInPixel = 0;
            stopwatch.Reset();
        }

        public static bool InitTrackingData(int levelId, bool charlie)
        {
            logger.Info("Launch level " + levelId);

            Initialize();

            if (!levels.ContainsKey(levelId))
            {
                return false;
            }

            currentLevel = levels[levelId];
            currentLevel.charlie = charlie;

            TrackingData.stream = host.Streams.CreateGazePointDataStream();
            stream.GazePoint((gazePointX, gazePointY, _) => OnChange(gazePointX, gazePointY));

            duringTime.Restart();

            return true;
        }

        private static int CalculateDistanceBetween(CharliePosition a, CharliePosition b)
        {
            return Convert.ToInt32(Math.Sqrt(Math.Pow((b.x - a.x), 2) + Math.Pow((b.y - a.y), 2)));
        }

        private static void OnChange(double gazePointX, double gazePointY)
        {
            if (currentLevel != null)
            {
                CharliePosition newPos = new CharliePosition(gazePointX, gazePointY);

                if (previousPos != null)
                {
                    distanceInPixel += CalculateDistanceBetween(previousPos, newPos);
                }

                // Si on regarde au bon endroit
                if (IsInRangeX(gazePointX) && IsInRangeY(gazePointY))
                {
                    if (!stopwatch.IsRunning)
                    {
                        stopwatch.Start();
                    }
                    else if (stopwatch.Elapsed.TotalSeconds >= TIME_TO_FIND)
                    {
                        stream.IsEnabled = false;
                        CharlieFind();
                    }
                }
                else
                {
                    if (stopwatch.IsRunning)
                    {
                        stopwatch.Reset();
                    }
                }

                previousPos = newPos;
            }
        }

        private static void CharlieFind()
        {
            // Transmission de l'information au serveur NODE
            var client = new RestClient("http://localhost:3000/");
            var request = new RestRequest("/found/", Method.POST);
            // Json to post.
            duringTime.Stop();
            string jsonToSend = "{ \"status\": \"OK\", \"timeSpend\":  \"" + Math.Round(duringTime.Elapsed.TotalSeconds) + "\", \"distance\":  \"" + distanceInPixel + "\" }";
            request.AddParameter("application/json; charset=utf-8", jsonToSend, ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            try
            {
                client.ExecuteAsync(request, response =>
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        logger.Info("CALL API NODE : OK");
                    }
                    else
                    {
                        logger.Error("CALL API NODE : Erreur");
                    }
                });
            }
            catch (Exception error)
            {
                logger.Error(error);
            }
            
            Initialize();
            logger.Info("Charlie find");
        }
    }
}
