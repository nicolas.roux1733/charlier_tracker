﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;
using NLog;

namespace api
{
    class Program
    {
        const string url = "http://localhost:9000";

        private static Logger logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            using (WebApp.Start<Startup>(url))
            {
                logger.Info("Server started at:" + url);
                Console.ReadLine();
            }
        }
    }
}
