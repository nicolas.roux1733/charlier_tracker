const express = require('express')
const uuidv1 = require('uuid/v1');
var bodyParser = require("body-parser");

const app = express()

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let find = false
let timeSpendScore = null
let distanceScore = null

function sseDemo(req, res) {
    let messageId = uuidv1();
    const intervalId = setInterval(() => {
        res.write(`id: ${messageId}\n`);
        res.write(`data: success `+find+` :`+timeSpendScore+`:`+distanceScore+`\n\n`)
        find = false
    }, 500);
    req.on('close', () => {
        clearInterval(intervalId);
    });
}

app.get('/success', (req, res) => {
    // SSE Setup
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Access-Control-Allow-Origin': 'http://localhost:5500',
    });
    res.write('\n')
    sseDemo(req, res)
});

app.post('/found', (req, res) => {
    find = true;
    timeSpendScore = req.body.timeSpend
    distanceScore = req.body.distance
    console.log(req.body.timeSpend);
    console.log(req.body.distance);
    res.json({find: 'find true'})
});

app.get('/', (req, res) => {
    res.json({currentRoute: "index"})
})

console.info('server started at localhost:3000');

app.listen(3000)