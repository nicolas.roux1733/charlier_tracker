@echo off
cd server\C#\api

REM ################################################################

echo Restoration des package nugets...

tools\nuget.exe restore api.sln >nul

if %errorlevel% == 0 (
  echo Restoration OK
) else (
  echo Erreur lors de la restoration des package nuget
  goto :end
)

REM ################################################################

echo Compilation du serveur C#...

if exist C:\Windows\Microsoft.NET\Framework\v4.0.30319 (
  C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe api.sln >nul
  if %errorlevel% == 0 (
	echo compilation OK
  ) else (
	echo Erreur lors de la compilation C#
	goto :end
  )
) else (
  where msbuild.exe
  if %errorlevel% == 0 (
	msbuild.exe api.snl >nul
	if %errorlevel% == 0 (
	  echo compilation OK
	) else (
	  echo La compilation nécésite l'instalation du Microsoft dotNET framework ^(\4.0.30319^) Si il est installé dans une autre version, ajoutez la commande 'msbuild' au PATH
	  goto :end
	)
  ) else (
	echo coucou
    goto :end
  )
)

REM ################################################################

cd ..\..\..\scripts

echo Lancement du serveur C#
START launch_cSharp_server.bat

echo Lancement du serveur nodeJS
START launch_node_js.bat

cd ..

echo Les 2 serveurs ont bien ete lance ! (vous pouvez quitter cette fenetre)

:end
pause