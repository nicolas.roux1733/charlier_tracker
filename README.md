
# Charlie Tracker

Charlie Tracker est un jeu basé sur un Eye Tracker.

Il est réalisé par 5 étudiants en licence professionnelle Dawin de Bordeaux :
Damien LECRONTE - Sylvain DUPUY - Nicolas ROUX - Anthony RUBIO - Benoit RODDE

Twitter : [@tracker_charlie](https://twitter.com/tracker_charlie)

## Architecture

Ce projet utilise 3 pôles :

* Un client en classique HTML/CSS/JS (qui doit tourner sur un serveur web !)

> Si vous êtes en local, mettre l'adresse de votre serveur Web dans `server/nodejs/app.js` (ligne 32) pour contourner la CORS policy :

```
'Access-Control-Allow-Origin': 'http://votreAdresse:port',
```

* Un serveur C# qui va communiquer avec l'eye tracker **localhost:9000**
* Un serveur nodeJS qui va permettre de transférer des données du C# vers le client **localhost:3000**

## Pré requis

Pour pouvoir utiliser cette application, il faut :

* Un eye tracker de la marque [Tobii](https://www.tobii.com/) (avec le [logiciel](https://gaming.tobii.com/getstarted/?bundle=tobii-core) installé)
* Windows : **Résolution 1080p en 125%**
<img src="https://gitlab.com/nicolas.roux1733/charlier_tracker/raw/master/img/windowsSettings.PNG" alt="Parametres windows" width="300"/>

* Avoir le Microsoft .NET framework installé (v4.0.30319)
* Un serveur web où cloner le projet (apache, Live server (visual studio code extension)...)

## Compilation et installation

Si vous avez tout les pré requis, vous pouvez lancer le projet à l'aide du script `run.bat` (ou double clic sur le script) :

```bash
run.bat
```

> Ce script va générer l'éxecutable du serveur C# puis lancer les 2 serveurs (C# et nodejs)

## Erreur

Si vous avez l'erreur ci-dessous alors que votre eye tracker est bien branché et installé (avec un eye tracking non récent par exemple):

<img src="https://gitlab.com/nicolas.roux1733/charlier_tracker/raw/master/img/error.PNG" alt="Parametres windows" width="700"/>

Vous pouvez supprimer le contrôle dans `server\C#\api\api\Controller\GameController.cs`, remplacez :

```csharp
public status.LaunchRecordStatus GetLaunchRecord(int id, bool charlie)
{
    if (TrackingData.host == null)
    {
        TrackingData.host = new Host();
    }

    var eyeTrackingStatus = TrackingData.host.States.GetEyeTrackingDeviceStatusAsync().Result;

    if (eyeTrackingStatus.IsValid)
    {
        switch (eyeTrackingStatus.Value)
        {
            case EyeTrackingDeviceStatus.Tracking:
                if (!TrackingData.InitTrackingData(id, charlie))
                {
                    logger.Error("Level does not exist");
                    return new status.LaunchRecordStatus("LEVEL_" + id + "_DOES_NOT_EXIST");
                };
                return new status.LaunchRecordStatus("OK");
            default:
                logger.Error("Tracker error");
                return new status.LaunchRecordStatus("TRACKER_ERROR");
        }
    }
    logger.Error("Unknow error");
    return new status.LaunchRecordStatus("ERROR");
}
```

par

```csharp
public status.LaunchRecordStatus GetLaunchRecord(int id, bool charlie)
{
    if (TrackingData.host == null)
    {
        TrackingData.host = new Host();
    }

    if (!TrackingData.InitTrackingData(id, charlie))
    {
        logger.Error("Level does not exist");
        return new status.LaunchRecordStatus("LEVEL_" + id + "_DOES_NOT_EXIST");
    };
    return new status.LaunchRecordStatus("OK");
}
```